This repository contains code for reinforcement learning in a finite state environment based on cart-pole dynamics.

![image info](./info/readme_pic-1.png)
![image info](info/readme_pic-2.png)
![image info](info/readme_pic-3.png)
![image info](info/readme_pic-4.png)
![image info](info/readme_pic-5.png)